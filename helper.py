"""Helper functions for Hubbard model"""

import numpy as np

s0 = np.identity(2)
sx = np.array([[0, 1], [1, 0]])
sy = np.array([[0, -1j], [1j, 0]])
sz = np.diag([1, -1])

sigmas = [sx, sy, sz]

def rotate_quantization_axis(mf_sol):
    """Rotates magnetization direction such that first site is magnetized in
    z-direction"""
    
    mf_sol_mat = mf_sol[()]

    magnetization_direction = np.zeros(3)
    for i, sigma in enumerate(sigmas):
        magnetization_direction[i] += np.trace(sigma @ mf_sol_mat[:2, :2]).real

    #magnetization_direction /= np.linalg.norm(magnetization_direction)
    reference_magnetization = np.transpose(sigmas, (1, 2, 0)) @ magnetization_direction

    _ , evecs = np.linalg.eigh(reference_magnetization)

    num_sites = mf_sol_mat.shape[0]//2

    return { (): np.kron(np.eye(num_sites), evecs).T.conj() @ mf_sol_mat @ np.kron(np.eye(num_sites), evecs) }
